Name:           snap7
Version:        1.4.2
Release:        1
Summary:        Step7 Open Source Ethernet Communication Suite
BuildRoot:      %_topdir/tesmp

License:        LGPL3
URL:            http://snap7.sourceforge.net/

BuildRequires:  gcc gcc-c++ make

Prefix: /usr
%define _rpmdir %_topdir/RPMS
%define _srcrpmdir %_topdir/SRPMS
%debug_package

%description
Step7 Open Source Ethernet Communication Suite with extra

%prep

%build
make %{?_smp_mflags} -C %_topdir/../../build/unix -f x86_64_linux.mk


%install
rm -rf $RPM_BUILD_ROOT
install -d $RPM_BUILD_ROOT/usr/lib64
install -m755 %_topdir/../../build/bin/x86_64-linux/libsnap7.so $RPM_BUILD_ROOT/usr/lib64
install -m644 %_topdir/../../build/bin/x86_64-linux/libsnap7.a $RPM_BUILD_ROOT/usr/lib64

install -d $RPM_BUILD_ROOT/usr/include/snap7
install -m644 %_topdir/../../src/lib/*.h $RPM_BUILD_ROOT/usr/include/snap7
install -m644 %_topdir/../../src/core/*.h $RPM_BUILD_ROOT/usr/include/snap7
install -m644 %_topdir/../../src/sys/*.h $RPM_BUILD_ROOT/usr/include/snap7
install -m644 %_topdir/../../extra/*.h $RPM_BUILD_ROOT/usr/include/snap7

%files
%defattr(-,root,root,-)
/usr/lib64/libsnap7.so
/usr/lib64/libsnap7.a
%dir /usr/include/snap7
/usr/include/snap7/s7.h
/usr/include/snap7/s7_client.h
/usr/include/snap7/s7_firmware.h
/usr/include/snap7/s7_isotcp.h
/usr/include/snap7/s7_micro_client.h
/usr/include/snap7/s7_partner.h
/usr/include/snap7/s7_peer.h
/usr/include/snap7/s7_server.h
/usr/include/snap7/s7_text.h
/usr/include/snap7/s7_types.h
/usr/include/snap7/snap7.h
/usr/include/snap7/snap7_libmain.h
/usr/include/snap7/snap_msgsock.h
/usr/include/snap7/snap_platform.h
/usr/include/snap7/snap_sysutils.h
/usr/include/snap7/snap_tcpsrvr.h
/usr/include/snap7/snap_threads.h
/usr/include/snap7/sol_threads.h
/usr/include/snap7/unix_threads.h
/usr/include/snap7/win_threads.h
